class Module {
  static type = 'Js!'
    
  constructor(options) {
    this.counter = options.counter
    this.status = options.status
    this.yers = options.yers
  }

  voice() {
    console.log('Hello!')
  }

}

const statistic = new Module({
  counter: 39.90,
  status: 201,
  yers: 2004
})


class Data extends Module { }

const dataI = new Data({
  name: 'Akbar',
  age: 17,
  hasTail: true
})